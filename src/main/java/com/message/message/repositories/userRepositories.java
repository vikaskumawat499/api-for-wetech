package com.message.message.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.message.message.models.userPOJO;

public interface userRepositories extends JpaRepository<userPOJO, Integer>{

}
