package com.message.message.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.message.message.models.messagePOJO;
import com.message.message.repositories.messageRepository;
// controller to send message and for other api related to message
@RestController
public class MessageController {
	
	@Autowired
	private messageRepository messageRepository;
	
	@RequestMapping(value="/sendMessage/{sender_id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> sendMessage(messagePOJO pojo,@PathVariable String sender_id) {
		pojo.setSender_id(sender_id);
		messageRepository.save(pojo);
		return new ResponseEntity<>(pojo,HttpStatus.OK);
	}
	
	@RequestMapping("/getAllMessage")
	public List<messagePOJO> getAllMessage(){
		return messageRepository.findAll();
	}
	
	@RequestMapping("/deleteMessage/{id}")
	public String deleteMessage(@PathVariable int id) {
		messageRepository.deleteById(id);
		return "Deleted" ;
	}
	
	@RequestMapping("/searchByDate/{id}")
	public List<messagePOJO> searchByDate(@PathVariable String id){
		List<messagePOJO> list = new ArrayList<>();
		messageRepository.findByDate(id).forEach(list::add);;
		return list;
		
	}
	
}