package com.message.message.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.message.message.models.userPOJO;
import com.message.message.repositories.userRepositories;
//controller to create user details and other api related to user
@RestController
public class UserController {
	
	@Autowired
	private userRepositories userRepositories;
	
	@RequestMapping("/getAllUser")
	public List<userPOJO> getAllUser(){
		return userRepositories.findAll();
	}
	
	@RequestMapping(value="/createUser")
	@ResponseStatus(HttpStatus.OK)
	public String creteUser(userPOJO pojo) {
		userRepositories.save(pojo);
		return "User Created";
	}
	
	@RequestMapping("/getOne/{id}")
	public userPOJO getOne(@PathVariable int id) {
		return userRepositories.getOne(id);
	}
}
