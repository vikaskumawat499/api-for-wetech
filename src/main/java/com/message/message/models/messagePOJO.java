package com.message.message.models;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="message")
public class messagePOJO {
	@Id  
	@GeneratedValue(
		    strategy= GenerationType.AUTO, 
		    generator="native"
		)
		@GenericGenerator(
		    name = "native", 
		    strategy = "native"
		)
	private int id;
	private String sender_id;
	private String reciever_id;
	private String message;
	private Timestamp date = new Timestamp(System.currentTimeMillis());
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSender_id() {
		return sender_id; 
	}
	public void setSender_id(String sender_id) {
		this.sender_id = sender_id;
	}
	public String getReciever_id() {
		return reciever_id;
	} 
	public void setReciever_id(String reciever_id) {
		this.reciever_id = reciever_id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
}